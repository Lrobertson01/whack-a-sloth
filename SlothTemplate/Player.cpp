#include "Player.h"

Player::Player(sf::Texture& playertexture, sf::Vector2u screensize)
{
    Sprite.setTexture(playertexture);

    Sprite.setPosition
    (screensize.x / 2 - playertexture.getSize().x / 2, screensize.y / 2 - playertexture.getSize().y / 2);

    Velocity.x = 0.0f;
    
    Velocity.y = 0.0f;

    speed = 300.0f;
    score = 0;
}

void Player::input()
{
    //Reset velocity at the start of each frame
    Velocity.x = 0.0f;

    Velocity.y = 0.0f;

    //movement
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        // left key is pressed: move our character
        Velocity.x = speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        // left key is pressed: move our character
        Velocity.x = -speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        // left key is pressed: move our character
        Velocity.y = speed;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        // left key is pressed: move our character
        Velocity.y = -speed;
    }



}

void Player::update(sf::Time frameTime)
{
    //set the character to move when they have a velocity
    Sprite.setPosition(Sprite.getPosition() + Velocity * frameTime.asSeconds());

}

void Player::reset(sf::Vector2u screensize)
{
    Sprite.setPosition
    (screensize.x / 2 - Sprite.getTexture()->getSize().x / 2, screensize.y / 2 - Sprite.getTexture()->getSize().x / 2);
}