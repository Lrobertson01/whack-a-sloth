#pragma once
#include<vector>
#include<SFML/Graphics.hpp>
class Sloth
{

	public:

	//constructor
	Sloth(std::vector < sf::Texture>& itemTextures, sf::Vector2u screensize);

	sf::Sprite sprite;
	int pointsvalue;
	sf::Time slothTime = sf::seconds(6.0f); //3 sloths can be active at a time
};