#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>

#include <cstdlib>
#include <time.h>
#include "Player.h"
#include "Sloth.h"

int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Whack A Sloth", sf::Style::Titlebar | sf::Style::Close);
	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	//Set variable to be Random via connecting to time
	srand(time(NULL));
	//set gameover variable
	bool gameover = false;

	// Player Sprite
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");

	Player playerObject(playerTexture, gameWindow.getSize());

	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	//sound effect/s
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/Pickup.wav");

	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");

	sf::Sound pickUpSound;
	pickUpSound.setBuffer(pickupSoundBuffer);

	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game Font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	sf::Text titleText;
	titleText.setFont(gameFont);
	titleText.setString("Whack A Sloth");
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Cyan);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);


	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Lewis Robertson");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 20, 60);


	// Gameover Text
	sf::Text restartText;
	restartText.setFont(gameFont);
	restartText.setString("Press R to restart");
	restartText.setCharacterSize(24);
	restartText.setFillColor(sf::Color::Cyan);
	restartText.setStyle(sf::Text::Bold | sf::Text::Italic);
	restartText.setPosition(gameWindow.getSize().x / 2 - restartText.getLocalBounds().width / 2, 250);

	// Gameover Text
	sf::Text quitText;
	quitText.setFont(gameFont);
	quitText.setString("Press Esc to quit");
	quitText.setCharacterSize(24);
	quitText.setFillColor(sf::Color::Cyan);
	quitText.setStyle(sf::Text::Bold | sf::Text::Italic);
	quitText.setPosition(gameWindow.getSize().x / 2 - quitText.getLocalBounds().width / 2, 350);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	sf::Time timeLimit = sf::seconds(20.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;


	sf::Texture holetexture;
	holetexture.loadFromFile("Assets/Graphics/hole.png");

	std::vector<sf::Sprite> Holes(9, sf::Sprite(holetexture));

	//I know this is really bad practice, but i cant think of a nicer method of doing it right now. 
	int row_x = 1;
	int row_y = 1;
	for (int I = 0; I < Holes.size(); I++)
	{
		Holes[I].setPosition(gameWindow.getSize().x / 3 * row_x - 400, gameWindow.getSize().y / 3 * row_y - 250);

		if (I == 2 || I == 5)
		{
			row_x = 1;
			row_y++;
		}

		else
		{
			row_x++;
		}
	}

	//Sloth Spawning
	sf::Time slothspawnduration = sf::seconds(2.0f);
	//create a timer for the time remaining in the game
	sf::Time slothspawnremaining = slothspawnduration;

	// sloths
	std::vector<Sloth> Sloths;

	std::vector<sf::Texture> slothTexture;


	slothTexture.push_back(sf::Texture());
	slothTexture[0].loadFromFile("Assets/Graphics/mole.png");


	// Load up a few random starting items - 1 of each type)
	Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));
	Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));
	Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));

	//Sets the initial starting sloths to go into the holes
	for (int i = 0; i < Sloths.size(); ++i) //loop to place each sloth into a respective hole
	{

		Sloths[i].sprite.setPosition(Holes[rand() % 9].getPosition());
	}

	// run the program as long as the window is open
	while (gameWindow.isOpen())
	{


		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				gameWindow.close();
		}


		//Inputs
		playerObject.input();

		//An easy way to exit out of the program
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}
		//An easy way to gameover
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			timeRemaining = timeRemaining - timeRemaining;
		}

		
		//Update loop
		sf::Time frameTime = gameClock.restart();
		timeRemaining = timeRemaining - frameTime;
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
		scoreText.setString("Score: " + std::to_string((int)playerObject.score));
		scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 20, 60); //reset the scoretext position because otherwise it does weird things and looks off

		playerObject.update(frameTime);

		slothspawnremaining = slothspawnremaining - frameTime;
		if (slothspawnremaining <= sf::seconds(0.0f))
		{
			Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));
			slothspawnremaining = slothspawnduration;
			int newSloth = Sloths.size() - 1;
			Sloths[newSloth].sprite.setPosition(Holes[rand() % 9].getPosition());
		}

		for (int i = Sloths.size() - 1; i >= 0; --i)
		{
			Sloths[i].slothTime = Sloths[i].slothTime - frameTime;

			//once a sloth has passed its expiration date it gets deleted in the same way as if it were collected
			if (Sloths[i].slothTime <= sf::seconds(0.0f))
			{
				Sloths.erase(Sloths.begin() + i);
			}
		}


		//If 2 sloths are in the same slot, move one of them to a unique slot
		for (int i = 0; i < Sloths.size(); ++i) //loop to place each sloth into a respective hole
		{
			for (int y = 0; y < Sloths.size(); ++y)
			{
				if (y != i) //prevents the loop infinitely looping by checking itself 
				{
					if (Sloths[i].sprite.getPosition() == Sloths[y].sprite.getPosition())
					{
						Sloths[i].sprite.setPosition(Holes[rand() % 9].getPosition());
						--y; //sets the loop to check that sloth again in their new position
					}
				}

			}
		}

		//collision
		sf::FloatRect playerBounds = playerObject.Sprite.getGlobalBounds();
		for (int i = Sloths.size() - 1; i >= 0; --i)
		{
			sf::FloatRect itemBounds = Sloths[i].sprite.getGlobalBounds();

			if (itemBounds.intersects(playerBounds)) //if the player has touched an item
			{
				playerObject.score += Sloths[i].pointsvalue; //add the points value of the coin we picked up to the total player score
				timeRemaining = timeRemaining + slothspawnduration; //adds 2 seconds to each sloth you intersect
				Sloths.erase(Sloths.begin() + i); //the item is removed from the list, thus removing its sprite. Basically, delete the item once picked up

				//play the pickup sound effect
				pickUpSound.play();
			}
		}

			//Game Ending
			if (timeRemaining.asSeconds() <= 0)
			{
				//set the time remaining to 0
				timeRemaining = sf::seconds(0);

				//check if this is the first time gameover is true, as to stop it looping
				if (gameover == false)
				{
					//Stop game music
					gameMusic.stop();
					//Play victory music
					victorySound.play();
					//set gameover to true
					gameover = true;
				}

			}

			//game restart
			if (gameover)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					//restart the game
					gameover = false;
					timeRemaining = timeLimit;
					gameMusic.play(); //as the initial playing of music is done within the setup, we have to put this in here

					//spawn 3 intial items and clear the screen of items from the last game
					Sloths.clear();
					Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));
					Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));
					Sloths.push_back(Sloth(slothTexture, gameWindow.getSize()));

					//Sets the initial starting sloths to go into the holes
					for (int i = 0; i < Sloths.size(); ++i) //loop to place each sloth into a respective hole
					{

						Sloths[i].sprite.setPosition(Holes[rand() % 9].getPosition());
					}

					//restart the player back to the middle
					playerObject.reset(gameWindow.getSize());

					//reset the score
					playerObject.score = 0;

				}

			}
			// clear the window with white color
			gameWindow.clear(sf::Color::Blue);


			// draw everything here...
			// window.draw(...);

			gameWindow.draw(titleText);
			gameWindow.draw(authorText);
			if (!gameover) 
			{
				gameWindow.draw(scoreText);
				gameWindow.draw(timerText);
				for (int i = 0; i < Holes.size(); ++i) //loop to draw each hole in the grid
				{
					gameWindow.draw(Holes[i]);
				}
				for (int i = 0; i < Sloths.size(); ++i) //loop to draw each sloth in Sloths
				{
					gameWindow.draw(Sloths[i].sprite);
				}
				gameWindow.draw(playerObject.Sprite);
			}
			if (gameover) 
			{
				gameWindow.draw(restartText);
				gameWindow.draw(quitText);
			}

			// end the current frame
			gameWindow.display();
		}

		return 0;
	}