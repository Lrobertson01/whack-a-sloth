#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

class Player
{
	public:

	//Constructor
	Player(sf::Texture& playertexture, sf::Vector2u screensize);

	//variables
	sf::Vector2f Velocity;
	sf::Sprite Sprite;
	float speed;
	int score;

	void input();
	void update(sf::Time frameTime);
	void reset(sf::Vector2u screenSize);


};

